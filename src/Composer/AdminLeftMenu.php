<?php
/**
 * Created by PhpStorm.
 * User: ldawn
 * Date: 2021/8/17
 */

namespace App\View\Composer;

use App\Managers\RbacA\RbacAAdminPowerManager;
use App\Managers\RbacA\RbacAMenuManager;
use App\Managers\RbacA\RbacAPowerManager;
use Illuminate\View\View;


class AdminLeftMenu
{
    public function compose(View $view)
    {
        $data = request()->all();
        $self_admin = request()->session()->get('self_admin');
        //默认赋值
        $data['admin_id'] = $self_admin->id;
        $admin_power_ids=request()->session()->get("power_ids");
        $all_power=RbacAPowerManager::getListByCon([],false)->toArray();
        $admin_all_power_ids=$admin_power_ids;

        foreach ($admin_power_ids as $admin_power_id){
            $admin_all_power_ids=RbacAPowerManager::getSonIds($all_power,$admin_power_ids,$admin_all_power_ids,$admin_power_id);
        }

        $all_powers=RbacAPowerManager::getListByCon(['status'=>1],false)->toArray();
        $url_arr=[];
        foreach ($all_powers as $all_power){
            $url_arr[$all_power['id']]=$all_power['url'];
        }
        $all_menus=RbacAMenuManager::getListByCon(['status'=>1],false)->toArray();
        $menu_list=RbacAMenuManager::getselfMenuList($admin_all_power_ids,$url_arr,$all_menus);
        $view->with(compact(['menu_list']));
    }
}
