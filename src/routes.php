<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\Admin\RbacA\RbacAPowerController;
use App\Http\Controllers\Web\Admin\RbacA\RbacARoleController;
use App\Http\Controllers\Web\Admin\RbacA\RbacAAdminRoleController;
use App\Http\Controllers\Web\Admin\RbacA\RbacAAdminPowerController;
use App\Http\Controllers\Web\Admin\RbacA\RbacAMenuController;

Route::group(['prefix' => '/admin', 'middleware' => ['web','BeforeRequest', 'CheckAdminLogin','ActivityHistory','RbacA']], function () {

    //权限
    Route::any('rbacAPower/index', [RbacAPowerController::class, 'index']);
    Route::any('rbacAPower/edit', [RbacAPowerController::class, 'edit']);
    Route::patch('rbacAPower/editInfoPost', [RbacAPowerController::class, 'editInfoPost']);
    Route::delete('rbacAPower/batchDel', [RbacAPowerController::class, 'batchDel']);
    Route::delete('rbacAPower/del', [RbacAPowerController::class, 'del']);
    Route::any('rbacAPower/select', [RbacAPowerController::class, 'select']);
    Route::get('rbacAPower/setStatus', [RbacAPowerController::class, 'setStatus']);

    Route::patch('rbacAAdminPower/editInfoPost', [RbacAAdminPowerController::class, 'editInfoPost']);

    //角色
    Route::any('rbacARole/index', [RbacARoleController::class, 'index']);
    Route::any('rbacARole/edit', [RbacARoleController::class, 'edit']);
    Route::patch('rbacARole/editInfoPost', [RbacARoleController::class, 'editInfoPost']);
    Route::get('rbacARole/select', [RbacARoleController::class, 'select']);

    Route::any('rbacAAdminRole/index', [RbacAAdminRoleController::class, 'index']);
    Route::patch('rbacAAdminRole/editInfoPost', [RbacAAdminRoleController::class, 'editInfoPost']);
    Route::delete('rbacAAdminRole/del', [RbacAAdminRoleController::class, 'del']);
    Route::delete('rbacAAdminRole/batchDel', [RbacAAdminRoleController::class, 'batchDel']);


    //菜单
    Route::any('rbacAMenu/index', [RbacAMenuController::class, 'index']);
    Route::get('rbacAMenu/edit', [RbacAMenuController::class, 'edit']);
    Route::patch('rbacAMenu/editInfoPost', [RbacAMenuController::class, 'editInfoPost']);
    Route::any('rbacAMenu/select', [RbacAMenuController::class, 'select']);
    Route::patch('rbacAMenu/seqEditPost', [RbacAMenuController::class, 'seqEditPost']);
    Route::delete('rbacAMenu/del', [RbacAMenuController::class, 'del']);
});