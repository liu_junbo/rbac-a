<?php

namespace  App\Http\Controllers\Web\Admin\RbacA;

use App\Components\Common\RequestValidator;
use App\Managers\RbacA\RbacAAdminRoleManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\ApiResponse;
use App\Managers\RbacA\RbacAPowerManager;
use App\Models\RbacA\RbacAAdminRole;
use Illuminate\Http\Request;

class RbacAAdminRoleController
{
    const  MODULAR='rbacAAdminRole';

    /*
    * 首页
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function index(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) && !UtilsFunction::isObjNull($data['search_word'])) {
            $search_word = $data['search_word'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'with'=>['role']
        );
        $rbac_a_admin_roles =RbacAAdminRoleManager::getListByCon($con_arr, true);
        foreach ($rbac_a_admin_roles as $rbac_a_admin_role) {
            $rbac_a_admin_role = RbacAAdminRoleManager::getInfoByLevel($rbac_a_admin_role, '');
        }

        return view('RbacA::rbacAAdminRole.index', ['self_admin' => $self_admin, 'datas' => $rbac_a_admin_roles, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }


    /*
    * 添加或编辑-post
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function editInfoPost(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;

        $rbac_a_admin_role = RbacAAdminRoleManager::getListByCon(['admin_id' => $data['other_id'], 'role_id' => $data['id']], false)->first();

        if ($rbac_a_admin_role) {
            return ApiResponse::makeResponse(ApiResponse::REQUEST_REPEAT, '', '当前用户已拥有该角色');
        }

        $rbac_a_admin_role = new RbacAAdminRole();
        RbacAAdminRoleManager::setInfo($rbac_a_admin_role,['admin_id' => $data['other_id'], 'role_id' => $data['id']]);
        RbacAAdminRoleManager::save($rbac_a_admin_role);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_admin_role);
    }


    /*
    * 删除
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function del(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_admin_role = RbacAAdminRoleManager::getById($data['id']);
        if (!$rbac_a_admin_role) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        RbacAAdminRoleManager::batchDelete([$data['id']]);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }

    /*
    * 批量删除
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function batchDel(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'ids' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $ids_arr = explode(',',rtrim($data['ids'],','));
        RbacAAdminRoleManager::batchDelete($ids_arr);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }
}


