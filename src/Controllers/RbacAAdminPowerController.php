<?php

namespace  App\Http\Controllers\Web\Admin\RbacA;

use App\Components\Common\RequestValidator;
use App\Managers\RbacA\RbacAAdminPowerManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\ApiResponse;
use App\Managers\RbacA\RbacAAdminRoleManager;
use App\Managers\RbacA\RbacARolePowerManager;
use App\Models\RbacA\RbacAAdminPower;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class RbacAAdminPowerController
{
    const  MODULAR='rbacAAdminPower';

    /*
    * 添加或编辑-post
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function editInfoPost(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $power_ids = [];

        foreach (Arr::get($data,'power_ids',[]) as $key_1 => $level_1) {
            if (!Arr::get($power_ids,trim($key_1,"'"))) {
                if (!is_array($level_1)) {
                    $power_ids[trim($key_1,"'")]=$level_1;
                    continue;
                }
                foreach ($level_1 as $key_2 => $level_2){
                    if (!Arr::get($power_ids,trim($key_2,"'"))) {
                        if (!is_array($level_2)) {
                            $power_ids[trim($key_2,"'")]=$level_2;
                            continue;
                        }
                    }else{
                        continue;
                    }

                    foreach ($level_2 as $key_3 => $level_3) {
                        $power_ids[trim($key_3, "'")] = $level_3;
                    }
                }
            }
        }

        $old_power_ids=RbacAAdminPowerManager::getListByCon(['admin_id'=>$data['admin_id'],'type'=>$data['type']],false,true)->pluck('power_id')->toArray();

        $del_power_ids=array_diff($old_power_ids,$power_ids);

        if ($del_power_ids != []) {
            $del_role_power_ids=RbacAAdminPowerManager::getListByCon(['admin_id'=>$data['admin_id'],'power_ids'=>$del_power_ids,'type'=>$data['type']],false,true)->pluck('id')->toArray();
            RbacAAdminPowerManager::batchDelete($del_role_power_ids);
        }

        $add_power_ids=array_diff($power_ids,$old_power_ids);
        if ($add_power_ids != []) {
            $add_power_arr=[];
            foreach ($add_power_ids as $add_power_id){
                array_push($add_power_arr,['power_id'=>$add_power_id,'admin_id'=>$data['admin_id'],'type'=>$data['type']]);
            }
            RbacAAdminPowerManager::batchInsert($add_power_arr);
        }
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, '');
    }
}


