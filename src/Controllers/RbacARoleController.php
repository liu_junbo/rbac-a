<?php

namespace  App\Http\Controllers\Web\Admin\RbacA;

use App\Components\Common\RequestValidator;
use App\Managers\RbacA\RbacARoleManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\ApiResponse;
use App\Managers\RbacA\RbacARolePowerManager;
use App\Models\RbacA\RbacARole;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Routing\Router;
use Illuminate\Support\Str;

class RbacARoleController
{
    const  MODULAR='rbacARole';

    /*
    * 首页
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function index(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) && !UtilsFunction::isObjNull($data['search_word'])) {
            $search_word = $data['search_word'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
        );
        $rbac_a_roles =RbacARoleManager::getListByCon($con_arr, true);
        foreach ($rbac_a_roles as $rbac_a_role) {
            $rbac_a_role = RbacARoleManager::getInfoByLevel($rbac_a_role, '');
        }

        return view('RbacA::rbacARole.index', ['self_admin' => $self_admin, 'datas' => $rbac_a_roles, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }

    /*
    * 编辑-get
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function edit(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        $item=0;
        //生成七牛token
        $upload_token = QNManager::uploadToken();
        $rbac_a_role = new RbacARole();
        if (array_key_exists('id', $data)) {
            $rbac_a_role = RbacARoleManager::getById($data['id']);
        }
        $rbac_a_role = RbacARoleManager::getInfoByLevel($rbac_a_role, "power_list");
        if (array_key_exists('item', $data)) {
            $item=$data['item'];
        }
        return view('RbacA::rbacARole.edit', ['self_admin' => $self_admin, 'data' => $rbac_a_role, 'upload_token' => $upload_token, 'item' => $item,'modular'=>self::MODULAR]);
    }


    /*
    * 添加或编辑-post
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function editInfoPost(Request $request)
    {
        $data = $request->all();

        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        $rbac_a_role = new RbacARole();
        //存在id是保存
        if (array_key_exists('id', $data) && !UtilsFunction::isObjNull($data['id'])) {
            $rbac_a_role = RbacARoleManager::getById($data['id']);
        }
        $data['admin_id'] = $self_admin['id'];
        $rbac_a_role = RbacARoleManager::setInfo($rbac_a_role, $data);
        RbacARoleManager::save($rbac_a_role);

        $power_ids = [];
        foreach (Arr::get($data,'power_ids',[]) as $key_1 => $level_1) {
            if (!Arr::get($power_ids,trim($key_1,"'"))) {
                if (!is_array($level_1)) {
                    $power_ids[trim($key_1,"'")]=$level_1;
                    continue;
                }
                foreach ($level_1 as $key_2 => $level_2){
                    if (!Arr::get($power_ids,trim($key_2,"'"))) {
                        if (!is_array($level_2)) {
                            $power_ids[trim($key_2,"'")]=$level_2;
                            continue;
                        }
                    }else{
                        continue;
                    }

                    foreach ($level_2 as $key_3 => $level_3) {
                            $power_ids[trim($key_3, "'")] = $level_3;
                    }
                }
            }
        }

        $old_power_ids=RbacARolePowerManager::getListByCon(['role_id'=>$rbac_a_role->id],false)->pluck('power_id')->toArray();

        $del_power_ids=array_diff($old_power_ids,$power_ids);

        if ($del_power_ids != []) {
            $del_role_power_ids=RbacARolePowerManager::getListByCon(['role_id'=>$rbac_a_role->id,'power_ids'=>$del_power_ids],false)->pluck('id')->toArray();
            RbacARolePowerManager::batchDelete($del_role_power_ids);
        }

        $add_power_ids=array_diff($power_ids,$old_power_ids);
        if ($add_power_ids != []) {
            $add_power_arr=[];
            foreach ($add_power_ids as $add_power_id){
             array_push($add_power_arr,['power_id'=>$add_power_id,'role_id'=>$rbac_a_role->id]);
            }
             RbacARolePowerManager::batchInsert($add_power_arr);
        }
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_role);
    }


    /*
    * 设置状态
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setStatus(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
            'status'=>'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_role = RbacARoleManager::getById($data['id']);
        if (!$rbac_a_role) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_role = RbacARoleManager::setInfo($rbac_a_role, $data);
        RbacARoleManager::save($rbac_a_role);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_role, "设置成功");
    }

    /*
    * 设置排序
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setSeq(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_role = RbacARoleManager::getById($data['id']);
        if (!$rbac_a_role) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_role = RbacARoleManager::setInfo($rbac_a_role, $data);
        RbacARoleManager::save($rbac_a_role);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_role, "设置成功");
    }


    /*
    * 批量删除
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function batchDelete(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'ids' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $ids_arr = explode(',',rtrim($data['ids'],','));
        RbacARoleManager::batchDelete($ids_arr);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }



    /*
    * 选择页
    *
    * By ldawn
    *
    * 2021/8/6
    *
    */
    public function select(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word=null;
        if (array_key_exists('status', $data) ) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) ) {
            $search_word = $data['search_word'];
        }
        if (array_key_exists('other_id', $data)) {
            $other_id = $data['other_id'];
        }
        if (array_key_exists('select_type', $data)) {
            $select_type = $data['select_type'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'other_id' => $other_id,
            'select_type' => $select_type,
        );

        $rbac_a_roles =RbacARoleManager::getListByCon($con_arr, true);
        foreach ($rbac_a_roles as $rbac_a_role) {
            $rbac_a_role = RbacARoleManager::getInfoByLevel($rbac_a_role, '');
        }

        return view('RbacA::rbacARole.select', ['self_admin' => $self_admin, 'datas' => $rbac_a_roles, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }

        /*
        * 排序编辑页
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEdit(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            return view('admin.rbacARole.seq_edit', ['self_admin' => $self_admin, 'seq_type' => $data['seq_type'],'modular'=>self::MODULAR]);
        }

        /*
        * 排序保存
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEditPost(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            //默认赋值
            $data['admin_id']=$self_admin->id;
            //合规校验
            $requestValidationResult = RequestValidator::validator($request->all(), [
                'id' => 'required',
                'seq_type'=>'required'
            ]);
            if ($requestValidationResult !== true) {
                return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
            }
            $rbac_a_role = RbacARoleManager::getById($data['$rbac_a_role_id']);
            if (!$rbac_a_role) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
            }

            RbacARoleManager::setInfo($rbac_a_role,$data);
            RbacARoleManager::save($rbac_a_role);
            return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_role, "设置成功");
        }
}


