<?php

namespace  App\Http\Controllers\Web\Admin\RbacA;

use App\Components\Common\RequestValidator;
use App\Managers\RbacA\RbacAMenuManager;
use App\Components\Common\QNManager;
use App\Components\Common\UtilsFunction;
use App\Components\Common\ApiResponse;
use App\Managers\RbacA\RbacAPowerManager;
use App\Models\RbacA\RbacAMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class RbacAMenuController
{
    const  MODULAR='rbacAMenu';

    /*
    * 首页
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function index(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        if (array_key_exists('status', $data) && !UtilsFunction::isObjNull($data['status'])) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data) && !UtilsFunction::isObjNull($data['search_word'])) {
            $search_word = $data['search_word'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
        );
        $rbac_a_menus =RbacAMenuManager::getListByCon($con_arr, false)->toArray();
        $rbac_a_menus=RbacAMenuManager::getMenuList($rbac_a_menus);
        return view('RbacA::rbacAMenu.index', ['self_admin' => $self_admin, 'datas' => $rbac_a_menus, 'con_arr' => $con_arr,'modular'=>self::MODULAR]);
    }

    /*
    * 编辑-get
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function edit(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        $item=0;
        //生成七牛token
        $upload_token = QNManager::uploadToken();
        $rbac_a_menu = new RbacAMenu();
        if (array_key_exists('id', $data)) {
            $rbac_a_menu = RbacAMenuManager::getById($data['id']);
        }
        $rbac_a_menu = RbacAMenuManager::getInfoByLevel($rbac_a_menu, "menu,power");
        if (array_key_exists('item', $data)) {
            $item=$data['item'];
        }
        return view('RbacA::rbacAMenu.edit', ['self_admin' => $self_admin, 'data' => $rbac_a_menu, 'upload_token' => $upload_token, 'item' => $item,'modular'=>self::MODULAR]);
    }


    /*
    * 添加或编辑-post
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function editInfoPost(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        $rbac_a_menu = new RbacAMenu();
        //存在id是保存
        if (array_key_exists('id', $data) && !UtilsFunction::isObjNull($data['id'])) {
            $rbac_a_menu = RbacAMenuManager::getById($data['id']);
        }
        $data['admin_id'] = $self_admin['id'];
        if($data['id'] && $data['id']==$data['father_id']){
            return ApiResponse::makeResponse(ApiResponse::LOGIC_ERROR, '','上级菜单不能是自己');
        }


        $rbac_a_menu = RbacAMenuManager::setInfo($rbac_a_menu, $data);
        RbacAMenuManager::save($rbac_a_menu);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_menu);
    }


    /*
    * 设置状态
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setStatus(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
            'status'=>'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_menu = RbacAMenuManager::getById($data['id']);
        if (!$rbac_a_menu) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_menu = RbacAMenuManager::setInfo($rbac_a_menu, $data);
        RbacAMenuManager::save($rbac_a_menu);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_menu, "设置成功");
    }

    /*
    * 设置排序
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function setSeq(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //默认赋值
        $data['admin_id']=$self_admin->id;
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required'
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $rbac_a_menu = RbacAMenuManager::getById($data['id']);
        if (!$rbac_a_menu) {
            return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
        }
        $rbac_a_menu = RbacAMenuManager::setInfo($rbac_a_menu, $data);
        RbacAMenuManager::save($rbac_a_menu);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_menu, "设置成功");
    }

    /*
    * 删除
    *
    * By ldawn
    *
    * 2021/8/18
    */
    public function del(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'id' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $son_rbaca_menu=RbacAMenuManager::getListByCon(['father_id'=>$data['id']],false,true)->first();
        if($son_rbaca_menu){
            return ApiResponse::makeResponse(ApiResponse::DELETE_FAILED, '','请删除全部下级菜单后在删除本菜单');
        }
        RbacAMenuManager::batchDelete([$data['id']]);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }

    /*
    * 批量删除
    *
    * By ldawn
    *
    * 2021/8/6
    */
    public function batchDelete(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //合规校验
        $requestValidationResult = RequestValidator::validator($request->all(), [
            'ids' => 'required',
        ]);
        if ($requestValidationResult !== true) {
            return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
        }
        $ids_arr = explode(',',rtrim($data['ids'],','));
        RbacAMenuManager::batchDelete($ids_arr);
        return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, null, "删除成功");
    }



    /*
    * 选择页
    *
    * By ldawn
    *
    * 2021/8/6
    *
    */

    public function select(Request $request)
    {
        $data = $request->all();
        $self_admin = $request->session()->get('self_admin');
        //相关搜素条件
        $status = null;
        $search_word = null;
        $other_id = null;
        $select_type = null;
        if (array_key_exists('status', $data)) {
            $status = $data['status'];
        }
        if (array_key_exists('search_word', $data)) {
            $search_word = $data['search_word'];
        }
        if (array_key_exists('other_id', $data)) {
            $other_id = $data['other_id'];
        }
        if (array_key_exists('select_type', $data)) {
            $select_type = $data['select_type'];
        }
        $con_arr = array(
            'status' => $status,
            'search_word' => $search_word,
            'other_id' => $other_id,
            'select_type' => $select_type,
        );

        $rbac_a_menus = RbacAMenuManager::getListByCon($con_arr, true);
        foreach ($rbac_a_menus as $rbac_a_menu) {
            $rbac_a_menu = RbacAMenuManager::getInfoByLevel($rbac_a_menu, '');
        }

        return view('RbacA::rbacAMenu.select', ['self_admin' => $self_admin, 'datas' => $rbac_a_menus, 'con_arr' => $con_arr, 'modular' => self::MODULAR]);
    }
        /*
        * 排序编辑页
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEdit(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            return view('admin.rbacAMenu.seq_edit', ['self_admin' => $self_admin, 'seq_type' => $data['seq_type'],'modular'=>self::MODULAR]);
        }

        /*
        * 排序保存
        *
        * By ldawn
        *
        * 2021/8/6
        *
        */
        public function seqEditPost(Request $request)
        {
            $data = $request->all();
            $self_admin = $request->session()->get('self_admin');
            //默认赋值
            $data['admin_id'] = $self_admin->id;
            //合规校验
            $requestValidationResult = RequestValidator::validator($request->all(), [
                'id' => 'required',
            ]);
            if ($requestValidationResult !== true) {
                return ApiResponse::makeResponse(ApiResponse::MISSING_PARAM, $requestValidationResult);
            }
            $rbac_a_menu = RbacAMenuManager::getById($data['id']);
            if (!$rbac_a_menu) {
                return ApiResponse::makeResponse(ApiResponse::INNER_ERROR, null, "未找到信息");
            }

            RbacAMenuManager::setInfo($rbac_a_menu, $data);
            RbacAMenuManager::save($rbac_a_menu);
            return ApiResponse::makeResponse(ApiResponse::SUCCESS_CODE, $rbac_a_menu, "设置成功");
        }
}


