<?php

/**
 * 后台权限管理中间件
 */

namespace Ldawn\RbacA\Middleware;

use App\Components\Common\ApiResponse;
use App\Managers\RbacA\RbacAPowerManager;
use Closure;

class RbacAMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!config('ldawn.rbac-a.status')){
            return $next($request);
        }
        $power_ids=$request->session()->get("power_ids");
        $path=ltrim($request->getPathInfo(),'/');
        if ($path == 'admin' || $path == "admin/index") {
            return $next($request);
        }
        $power = RbacAPowerManager::getListByCon(['url' => $path], false, true)->first();
        if(!$power){
            return ApiResponse::makeResponse(ApiResponse::PERMISSION_DENIED,'',  "权限不存在");
        }
        if(!in_array($power->id,$power_ids)){
            return ApiResponse::makeResponse(ApiResponse::PERMISSION_DENIED,'',  "本用户此项操作权限");
        }
        return $next($request);
    }
}
