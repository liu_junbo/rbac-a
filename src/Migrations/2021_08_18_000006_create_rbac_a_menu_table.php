<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbacAMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rbac_a_menu')) {
            Schema::create('rbac_a_menu', function (Blueprint $table) {
                $table->bigIncrements('id');
            });
            \DB::statement("alter table rbac_a_menu comment='rbac-a-菜单'");
        }

        Schema::table('rbac_a_menu', function (Blueprint $table) {
            if (!Schema::hasColumn('rbac_a_menu', 'name')) {
                $table->string('name',32)->nullable()->comment('名称');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'father_id')) {
                $table->integer('father_id')->default(0)->nullable()->comment('上级id');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'type')) {
                $table->tinyInteger('type')->default(1)->nullable()->comment('1-左侧菜单;2-顶部菜单');
            };
            if (!Schema::hasColumn('rbac_a_menu', 'power_id')) {
                $table->integer('power_id')->nullable()->comment('权限id');
            };
            if (!Schema::hasColumn('rbac_a_menu', 'status')) {
                $table->tinyInteger('status')->default(1)->nullable()->comment('状态 0:失效；1:生效');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'seq')) {
                $table->integer('seq')->default(99)->nullable()->comment('排序 由大到小排列');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'created_at')) {
                $table->dateTime('created_at')->nullable()->comment('创建时间');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'updated_at')) {
                $table->dateTime('updated_at')->nullable()->comment('修改时间');
            };

            if (!Schema::hasColumn('rbac_a_menu', 'deleted_at')) {
                $table->dateTime('deleted_at')->nullable()->comment('删除时间');
            };
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbac_a_menu');
    }
}
