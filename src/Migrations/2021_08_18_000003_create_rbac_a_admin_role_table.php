<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbacAAdminRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rbac_a_admin_role')) {
            Schema::create('rbac_a_admin_role', function (Blueprint $table) {
                $table->bigIncrements('id');
            });
            \DB::statement("alter table rbac_a_admin_role comment='rbac-a-管理员角色'");
        }

        Schema::table('rbac_a_admin_role', function (Blueprint $table) {
            if (!Schema::hasColumn('rbac_a_admin_role', 'admin_id')) {
                $table->integer('admin_id')->nullable()->comment('admin表id');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'role_id')) {
                $table->integer('role_id')->nullable()->comment('role表id');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'status')) {
                $table->tinyInteger('status')->default(1)->nullable()->comment('状态 0:失效；1:生效');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'seq')) {
                $table->integer('seq')->default(99)->nullable()->comment('排序 由大到小排列');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'created_at')) {
                $table->dateTime('created_at')->nullable()->comment('创建时间');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'updated_at')) {
                $table->dateTime('updated_at')->nullable()->comment('修改时间');
            };

            if (!Schema::hasColumn('rbac_a_admin_role', 'deleted_at')) {
                $table->dateTime('deleted_at')->nullable()->comment('删除时间');
            };
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbac_a_admin_role');
    }
}
