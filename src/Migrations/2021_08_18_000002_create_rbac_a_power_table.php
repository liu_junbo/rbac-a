<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbacAPowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rbac_a_power')) {
            Schema::create('rbac_a_power', function (Blueprint $table) {
                $table->bigIncrements('id');
            });
            \DB::statement("alter table rbac_a_power comment='Rbac-a-权限'");
        }

        Schema::table('rbac_a_power', function (Blueprint $table) {
            if (!Schema::hasColumn('rbac_a_power', 'name')) {
                $table->string('name',32)->nullable()->comment('名称');
            };

            if (!Schema::hasColumn('rbac_a_power', 'type')) {
                $table->tinyInteger('type')->default(3)->nullable()->comment('1-全局权限;2-模块完整权限;3-普通权限');
            };

            if (!Schema::hasColumn('rbac_a_power', 'platform')) {
                $table->string('platform',32)->default('')->nullable()->comment('平台');
            };

            if (!Schema::hasColumn('rbac_a_power', 'modular')) {
                $table->string('modular',32)->default('')->nullable()->comment('模块');
            };

            if (!Schema::hasColumn('rbac_a_power', 'father_id')) {
                $table->integer('father_id')->default(0)->nullable()->comment('父级id');
            };

            if (!Schema::hasColumn('rbac_a_power', 'url')) {
                $table->string('url')->default('')->nullable()->comment('链接');
            };

            if (!Schema::hasColumn('rbac_a_power', 'status')) {
                $table->tinyInteger('status')->default(1)->nullable()->comment('状态 0:失效；1:生效');
            };

            if (!Schema::hasColumn('rbac_a_power', 'seq')) {
                $table->integer('seq')->default(99)->nullable()->comment('排序 由大到小排列');
            };

            if (!Schema::hasColumn('rbac_a_power', 'created_at')) {
                $table->dateTime('created_at')->nullable()->comment('创建时间');
            };

            if (!Schema::hasColumn('rbac_a_power', 'updated_at')) {
                $table->dateTime('updated_at')->nullable()->comment('修改时间');
            };

            if (!Schema::hasColumn('rbac_a_power', 'deleted_at')) {
                $table->dateTime('deleted_at')->nullable()->comment('删除时间');
            };
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbac_a_power');
    }
}
