<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbacARolePowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rbac_a_role_power')) {
            Schema::create('rbac_a_role_power', function (Blueprint $table) {
                $table->bigIncrements('id');
            });
            \DB::statement("alter table rbac_a_role_power comment='rbac-a-角色权限'");
        }

        Schema::table('rbac_a_role_power', function (Blueprint $table) {
            if (!Schema::hasColumn('rbac_a_role_power', 'role_id')) {
                $table->integer('role_id')->nullable()->comment('角色id');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'father_id')) {
                $table->integer('power_id')->nullable()->comment('权限id');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'status')) {
                $table->tinyInteger('status')->default(1)->nullable()->comment('状态 0:失效；1:生效');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'seq')) {
                $table->integer('seq')->default(99)->nullable()->comment('排序 由大到小排列');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'created_at')) {
                $table->dateTime('created_at')->nullable()->comment('创建时间');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'updated_at')) {
                $table->dateTime('updated_at')->nullable()->comment('修改时间');
            };

            if (!Schema::hasColumn('rbac_a_role_power', 'deleted_at')) {
                $table->dateTime('deleted_at')->nullable()->comment('删除时间');
            };
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbac_a_role_power');
    }
}
