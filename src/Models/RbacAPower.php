<?php

/**
* Created by PhpStorm.
* User: ldawn
* Date: 2021/8/6
*/
namespace App\Models\RbacA;

use App\Models\Common\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RbacAPower extends BaseModel
{
    use SoftDeletes;
    protected $table ='rbac_a_power';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    /*
    * 进行类型转换
    *
    * @var  array
    *
    */
    protected $casts = [
            ];

    const TYPE_ENUM_1="1";
    const TYPE_ENUM_2="2";
    const TYPE_ENUM_3="3";

    const TYPE_ENUM_ARR=
        [
            self::TYPE_ENUM_1=>"全局权限",
            self::TYPE_ENUM_2=>"模块完整权限",
            self::TYPE_ENUM_3=>"普通权限",
        ];
}

