<?php

/**
* Created by PhpStorm.
* User: ldawn
* Date: 2021/8/6
*/
namespace App\Models\RbacA;

use App\Models\Common\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RbacAAdminRole extends BaseModel
{

    protected $table ='rbac_a_admin_role';
    public $timestamps = true;

    /*
    * 进行类型转换
    *
    * @var  array
    *
    */
    protected $casts = [
            ];

    public function role()
    {
        return $this->hasOne('App\Models\RbacA\RbacARole','id','role_id');
    }
}

