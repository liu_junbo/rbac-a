@if(isset($c_disabled))
<style>
    .layui-form-checkbox{
        pointer-events:none !important;
    }
</style>
@endif
<div class="layui-form-item" id="power">
    <label class="layui-form-label" style="width:150px !important">权限</label>
    <div class="layui-input-block " @if(isset($c_disabled)) style="cursor: not-allowed" @endif>
        @foreach($data->all_powers as $item_1)
            <div>
                <input type="checkbox" name="power_ids['{{$item_1['id']}}']"
                       title={{$item_1['name']}} value={{$item_1['id']}} @if(in_array($item_1['id'],$data->power_ids)) checked
                       @endif lay-filter="power" class="level_1 power_{{$item_1['id']}} " >

                @foreach($item_1['son_powers'] as $item_2)
                    <div style="margin-left:146px">
                        <input type="checkbox" name="power_ids[{{$item_1['id']}}]['{{$item_2['id']}}']"
                               title={{$item_2['name']}} value={{$item_2['id']}} @if(in_array($item_1['id'],$data->power_ids)) checked
                               @elseif(in_array($item_2['id'],$data->power_ids)) checked @endif lay-filter="power"
                               class="level_2 power_{{$item_1['id']}}_son power_{{$item_2['id']}}" >

                        <div style="margin-left:76px">
                            @foreach($item_2['son_powers'] as $item_3)
                                <input type="checkbox"
                                       name="power_ids[{{$item_1['id']}}][{{$item_2['id']}}]['{{$item_3['id']}}']"
                                       title={{$item_3['name']}} value={{$item_3['id']}}  @if(in_array($item_1['id'],$data->power_ids)) checked
                                       @elseif(in_array($item_2['id'],$data->power_ids)) checked
                                       @elseif(in_array($item_3['id'],$data->power_ids)) checked
                                       @endif lay-filter="power"
                                       class="level_3 power_{{$item_1['id']}}_son power_{{$item_2['id']}}_son power_{{$item_3['id']}}">
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</div>


@section('layer_init')
    @parent
    form.on('checkbox(power)', function (data) {
        var cb = data.elem;
        if(!data.elem.checked) {
            if($(cb).hasClass('level_3')){
                $(cb).parent().parent().children(".level_2:first-child").removeAttr("checked");
                $(cb).parent().parent().parent().children(".level_1:first-child").removeAttr("checked");
            }
            if($(cb).hasClass('level_2')){
                $(cb).parent().parent().children(".level_1:first-child").removeAttr("checked");
                $(cb).parent().find(".level_3").removeAttr("checked");
            }
            if($(cb).hasClass('level_1')){
                $(cb).parent().parent().children(".level_1:first-child").removeAttr("checked");
                $(cb).parent().find(".level_3").removeAttr("checked");
                $(cb).parent().find(".level_2").removeAttr("checked");
            }
        }
        if(data.elem.checked) {
            $(".power_"+ data.value +"_son").prop('checked',"checked")
        }
        form.render('checkbox');
    });
@endsection

