<style>

</style>

<form id="search_form" class="layui-form" method="post"
      action="{{URL::asset('admin/admin/edit')}}?id={{$data->id}}&item=8&page={{$data->admin_roles->currentPage()}}">
    {{csrf_field()}}
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <button class="layui-btn layuiadmin-btn-forum-list" type=button
                    onclick="openPage('添加角色','{{URL::asset('admin/rbacARole/select?select_type=adminRole&other_id=').$data->id}}')">
                添加角色
            </button>
            @include('component.grid.batch_del',['modular'=>'rbacAAdminRole'])
        </div>
        <div>
            <table class="layui-table text-c" style="width: 100%;" lay-filter="demo">
                <thead>
                <th scope="col" colspan="100">
                    <span>共有<strong>{{$data->admin_roles->total()}}</strong> 条数据</span>
                </th>
                <tr>
                    <th class="text-c" width="20"><input lay-skin="primary" type="checkbox" id="checkall"
                                                         name="checkall" lay-filter="checkall"
                                                         value="checkall"/></th>
                    <th class="text-c" width="20">ID</th>
                    <th class="text-c" width="50">名称</th>
                    <th class="text-c" width="120">操作</th>
                </tr>
                </thead>
                <tbody class="table_box">
                @foreach($data->admin_roles as $admin_role)
                    <tr>
                        <td><input type="checkbox" name="s_ids" lay-skin="primary" value=" {{$admin_role->id}}">
                        </td>
                        <td>{{$admin_role->id}}</td>
                        <td>{{$admin_role->role->name}}</td>
                        <td>
                            <div>
                                <button class="layui-btn layui-btn-danger layui-btn-sm" type="button"
                                        onclick="del({{$admin_role->id}})">
                                    删除
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="">
            {{ $data->admin_roles->appends($item)->links() }}
        </div>
    </div>
</form>

@include('component.grid.init')
@include('component.grid.del',['modular'=>'rbacAAdminRole'])
<script type="text/javascript">

    // 入口函数
    $(function () {

    });

    /*
 * 页面刷新
 */
    function refresh() {
        $('#search_form')[0].reset();
        location.replace('{{URL::asset('/admin/'.$modular.'/index')}}');
    }
</script>

