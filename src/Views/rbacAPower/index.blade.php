@extends('admin.layouts.app')

@section('content')

    <style>

    </style>

    <form id="search_form" class="layui-form" method="post"
          action="{{URL::asset('admin/'.$modular.'/index')}}?page={{$datas->currentPage()}}">
        <div class="layui-fluid">
            <div class="layui-card">
                <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                    {{csrf_field()}}
                    <div class="layui-form-item">
                        {{-- 模块--}}
                        @include('component.grid.select',['c_title'=>'模块','c_name'=>'modular','c_array'=>config('ldawn.modular_list'),'c_value'=>$con_arr['modular']])

                        {{--关键字--}}
                        @include('component.grid.input',['c_title'=>'关键字','c_name'=>'search_word','c_value'=>$con_arr['search_word']])

                        {{--提交按钮--}}
                        @include('component.grid.submit')
                    </div>
                </div>

                <div class="layui-card-body">
                    <div style="padding-bottom: 10px;">
                        <button class="layui-btn layuiadmin-btn-forum-list" type=button
                                onclick="openPage('添加权限','{{URL::asset('admin/'.$modular.'/edit')}}')">
                            添加权限
                        </button>
                        @include('component.grid.batch_del')
                    </div>
                    <div>
                        <table class="layui-table text-c" style="width: 100%;" lay-filter="demo">
                            <thead>
                            <th scope="col" colspan="100">
                                <span>共有<strong>{{$datas->total()}}</strong> 条数据</span>
                            </th>
                            <tr>
                                <th class="text-c" width="20"><input lay-skin="primary" type="checkbox" id="checkall" name="checkall" lay-filter="checkall" value="checkall"/></th>
                                <th class="text-c" width="20">ID</th>
                                <th class="text-c" width="40">模块名称</th>
                                <th class="text-c" width="50">名称</th>
                                <th class="text-c" width="50">类别</th>
                                <th class="text-c" width="50">url</th>
                                <th class="text-c" width="120">操作</th>
                            </tr>
                            </thead>
                            <tbody class="table_box">
                            @foreach($datas as $data)
                                <tr>
                                    <td><input type="checkbox" name="s_ids"  lay-skin="primary" value=" {{$data->id}}"></td>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->modular_str}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->type_str}}</td>
                                    <td>{{$data->url}}</td>
                                    <td>
                                        <div>
                                            @if($data->status == \App\Components\Common\UtilsConst::COMMON_STATUS_VALID)
                                                <button class="layui-btn layui-btn-sm layui-btn-normal" onclick="stop(this,{{$data->id}})" type="button">
                                                    已启用
                                                </button>
                                            @else
                                                <button class="layui-btn layui-btn-sm layui-btn-warm" onclick="start(this,{{$data->id}})" type="button">
                                                    已停用
                                                </button>
                                            @endif
                                            <button class="layui-btn layui-btn-sm" type="button"
                                                    onclick="openPage('编辑活动','{{URL::asset('admin/'.$modular.'/edit')}}?id={{$data->id}}')">
                                                编辑
                                            </button>
                                            <button class="layui-btn layui-btn-danger layui-btn-sm" type="button"
                                                    onclick="del({{$data->id}})">
                                                删除
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        {{ $datas->appends($con_arr)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')

    @include('component.grid.init')
    @include('component.grid.set_status')
    @include('component.grid.del')
    <script type="text/javascript">

        // 入口函数
        $(function () {

        });

        /*
     * 页面刷新
     */
        function refresh() {
            $('#search_form')[0].reset();
            location.replace('{{URL::asset('/admin/'.$modular.'/index')}}');
        }
    </script>
@endsection
