<style>

</style>
<div class="layui-card-body" pad15>
    <div class="layui-form" lay-filter="">

        @include('component.form.input_hidden',['c_name'=>'id','c_value'=>$data->id])

        @include('component.form.input',['c_title'=>'名称','c_name'=>'name','c_value'=>$data->name,'c_desc'=>'不可为空，请输入名称'])

        @include('component.form.select',['c_title'=>'权限类型','c_name'=>'type','c_value'=>$data->type,'c_array'=>\App\Models\RbacA\RbacAPower::TYPE_ENUM_ARR ])

        @include('component.form.input_select',
        ['c_title'=>'上级权限','c_name'=>'father','c_father'=>'father_id','c_id_value'=>$data->father_id,'c_name_value'=>isset($data->father->name)?$data->father->name:"",'c_verify'=>"",'c_url'=>URL::asset('admin/'.$modular.'/select')."?other_id=&select_type=father"])

        @include('component.form.input',['c_title'=>'url','c_name'=>'url','c_value'=>$data->url,'c_desc'=>'权限链接','c_width'=>'300px','c_verify'=>''])


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="LAY-form-edit">保存信息</button>
                <button type="reset" class="layui-btn layui-btn-primary" onclick="reloadPage();">
                    重新填写
                </button>
                <button class="layui-btn layui-btn-primary" onclick="back();">
                    返回
                </button>
            </div>
        </div>
    </div>
</div>
@section('layer_init')
    @parent
    form.on('select(type)', function(data){
        changeType(data.value)
    })
@endsection
@include('component.form.init',['form_url'=>URL::asset('/admin/'.$modular.'/editInfoPost')])

<script type="text/javascript">
var type="{{$data->type}}";
changeType(type);
    function  changeType(type){
        switch (type) {
            case '1':
                $("#url_box").addClass('hidden');
                $('#modular_box').addClass('hidden');
                $('#father_id_box').addClass('hidden');
                break;

            case '2':
                $('#modular_box').removeClass('hidden');
                $('#url_box').addClass('hidden');
                $('#father_id_box').removeClass('hidden');
                break;

            case '3':
                $('#modular_box').removeClass('hidden');
                $('#url_box').removeClass('hidden');
                $('#father_id_box').removeClass('hidden');
                break;
        }
    }
</script>
