@extends('admin.layouts.app')

@section('content')

    <style>

    </style>
    <form id="search_form" class="layui-form" method="post"
          action="{{URL::asset('admin/'.$modular.'/select')}}?page={{$datas->currentPage()}}&other_id={{$con_arr['other_id']}}&select_type={{$con_arr['select_type']}}">
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">

                    {{csrf_field()}}
                    <div class="layui-form-item">
                        {{--关键字--}}
                        @include('component.grid.input',['c_title'=>'关键字','c_name'=>'search_word','c_value'=>$con_arr['search_word']])
                        {{--提交按钮--}}
                        @include('component.grid.submit')
                    </div>

            </div>

            <div class="layui-card-body">
                <div>
                    <table class="layui-table text-c" style="width: 100%;">
                        <thead>
                        <th scope="col" colspan="100">
                            <span>共有<strong>{{$datas->total()}}</strong> 条数据</span>
                        </th>
                        <tr>
                            <th class="text-c" width="40">角色名称</th>
                            <th class="text-c" width="120">操作</th>
                        </tr>
                        </thead>
                        <tbody class="table_box">
                        @foreach($datas as $data)
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>
                                    <div>
                                        <button class="layui-btn layui-btn-sm"  type="button"
                                                onclick="select('{{$data->id}}','{{$data->name}}')">
                                            选择
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="">
                    <div class="">
                        {{ $datas->appends($con_arr)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection

@section('script')
    @include('component.select_grid.init')
    <script type="text/javascript">
        /*
         * 页面刷新
         *
         * By ldawn
         *
         */
        function refresh() {
            $('#search_form')[0].reset();
            location.replace('{{URL::asset('/admin/'.$modular.'/select?select_type=')}}'+ '{{$con_arr['select_type']}}' +'&other_id=' + '{{$con_arr['other_id']}}');
        }

        /*选择*/
        function select(id,name){
            switch ("{{$con_arr['select_type']}}") {
                case 'adminRole':
                    selectAjax(id,"{{URL::asset('/admin/rbacAAdminRole/editInfoPost')}}");
                    break;
                default:
                    selectBackInfo(id,name,'father');
                    break;
            }
        }
    </script>
@endsection
