<style>

</style>
<div class="layui-card-body" pad15>
    <div class="layui-form" lay-filter="">

        @include('component.form.input_hidden',['c_name'=>'admin_id','c_value'=>$data->id])
        @include('component.form.input_hidden',['c_name'=>'type','c_value'=>1])
        @include('RbacA::component.checkbox_power')


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="LAY-form-edit">保存信息</button>
                <button type="reset" class="layui-btn layui-btn-primary" onclick="reloadPage();">
                    重新填写
                </button>
                <button class="layui-btn layui-btn-primary" onclick="back();">
                    返回
                </button>
            </div>
        </div>
    </div>
</div>


@include('component.form.init',['form_url'=>URL::asset('/admin/rbacAAdminPower/editInfoPost'),'c_no_back'=>1])

<script type="text/javascript">

</script>
