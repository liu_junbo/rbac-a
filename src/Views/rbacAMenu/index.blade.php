@extends('admin.layouts.app')

@section('content')

    <style>

    </style>

    <form id="search_form" class="layui-form" method="post"
          action="{{URL::asset('admin/'.$modular.'/index')}}">
        <div class="layui-fluid">
            <div class="layui-card">
                <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                    {{csrf_field()}}
                </div>

                <div class="layui-card-body">
                    <div style="padding-bottom: 10px;">
                        <button class="layui-btn layuiadmin-btn-forum-list" type=button
                                onclick="openPage('添加菜单','{{URL::asset('admin/'.$modular.'/edit')}}')">
                            添加菜单
                        </button>
                    </div>
                    <div>
                        <table class="layui-table text-c" style="width: 100%;" lay-filter="demo">
                            <thead>
                            <th scope="col" colspan="100">
                            </th>
                            <tr>
                                <th class="text-c" width="50">一级菜单</th>
                                <th class="text-c" width="50">二级菜单</th>
                                <th class="text-c" width="50">排序</th>
                                <th class="text-c" width="120">操作</th>
                            </tr>
                            </thead>
                            <tbody class="table_box">
                            @foreach($datas as $one_menu)
                                <tr>
                                    <td>{{$one_menu['name']}}</td>
                                    <td></td>
                                    <td><input value="{{$one_menu['seq']}}" class="seq_input seq"
                                               onblur="seqEdit(this,{{$one_menu['id']}})"></td>
                                    <td>
                                        <div>
                                            <button class="layui-btn layui-btn-sm" type="button"
                                                    onclick="openPage('编辑菜单','{{URL::asset('admin/'.$modular.'/edit')}}?id={{$one_menu['id']}}')">
                                                编辑
                                            </button>
                                            <button class="layui-btn layui-btn-danger layui-btn-sm" type="button"
                                                    onclick="del({{$one_menu['id']}})">
                                                删除
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($one_menu['son_menu'] as $two_menu)
                                    <tr>
                                        <td></td>
                                        <td>{{$two_menu['name']}}</td>
                                        <td><input value="{{$two_menu['seq']}}" class="seq_input seq"
                                                   onblur="seqEdit(this,{{$two_menu['id']}})"></td>
                                        <td>
                                            <div>
                                                <button class="layui-btn layui-btn-sm" type="button"
                                                        onclick="openPage('编辑菜单','{{URL::asset('admin/'.$modular.'/edit')}}?id={{$two_menu['id']}}')">
                                                    编辑
                                                </button>
                                                <button class="layui-btn layui-btn-danger layui-btn-sm" type="button"
                                                        onclick="del({{$two_menu['id']}})">
                                                    删除
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">

                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')

    @include('component.grid.init')
    @include('component.grid.set_seq')
    @include('component.grid.del')
    <script type="text/javascript">

        // 入口函数
        $(function () {

        });

        /*
     * 页面刷新
     */
        function refresh() {
            $('#search_form')[0].reset();
            location.replace('{{URL::asset('/admin/'.$modular.'/index')}}');
        }
    </script>
@endsection
