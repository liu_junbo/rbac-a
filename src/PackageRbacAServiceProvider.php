<?php

namespace Ldawn\RbacA;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class PackageRBACAServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Views', 'RbacA');
        $this->app->singleton('rbac-a:init', Commands\Init::class);
        $this->commands([
            'rbac-a:init',
        ]);
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
    }
}
