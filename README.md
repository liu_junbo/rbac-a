# rbac-a

#### 安装
1.composer require ldawn/rbac-a
2.php artisan migrate
3.php artisan rbaca:createData
4.在LoginController.php loginPost方法末尾处增加RbacAAdminPowerManager::putSession();
5.web路由管理后台组增加中间件RbacA
6.在后台首页侧边菜单加入 @include("RbacA::rbacAMenu.menu")
7.在app.php providers内新增App\Providers\ViewServiceProvider::class,
8.建立ViewServiceProvider文件boot()方法内加入$this->menu(); menu()方法：view()->composer(['RbacA::rbacAMenu.menu'], AdminLeftMenu::class);

